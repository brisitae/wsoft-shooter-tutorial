﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float zDistance = 10f;
    public float rotPercentage;
    Vector3 oldView;
    Vector3 newView;
    //public GameObject camera;
    // Use this for initialization
    void Start () {
        var mousePos = Input.mousePosition;
        newView = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, zDistance));
    }
	
	// Update is called once per frame
	void Update () {
        var mousePos = Input.mousePosition;
        oldView = newView;
        newView = Vector3.Lerp(oldView, Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, zDistance)), rotPercentage);
        //transform.rotation = Quaternion.FromToRotation(oldView, newView);
        transform.LookAt(newView);
    }
}
