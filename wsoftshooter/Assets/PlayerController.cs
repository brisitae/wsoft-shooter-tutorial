﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    
    // Public members
    public float moveSpeed = 20f;

    // Private memebers (private by default)
    Vector3 moveDirection;
    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {

        // Store 1s if keys are pressed else store 0s
        float keyA = Input.GetKey(KeyCode.A)? 1 : 0;
        float keyD = Input.GetKey(KeyCode.D) ? 1 : 0;
        float keyW = Input.GetKey(KeyCode.W) ? 1 : 0;
        float keyS = Input.GetKey(KeyCode.S) ? 1 : 0;

        // Store horizontal axis directions 
        moveDirection = new Vector3(keyD - keyA, 0, keyW - keyS);
        // Apply move speed while vertical direction is not applied
        moveDirection *= moveSpeed * Time.deltaTime;

        // Apply our speed vector to our rigibody's velocity physics
        rb.velocity = moveDirection;
    }
}
